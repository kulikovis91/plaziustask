﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PlaziusTask
{
    class Program
    {
        static Card[] cards = {
            new Card("Мельбурн", "Кельн"),
            new Card("Москва", "Париж"),
            new Card("Кельн", "Москва"),
            new Card("Рязань", "Щегры"),
            new Card("Прага", "Рязань"),
            new Card("Щегры", "Одинцово"),
            new Card("Париж", "Прага")
        };
                      
        static void PrintCards(Card[] cards)
        {
            foreach (var card in cards)
            {
                Console.WriteLine($"{card.LocationFrom} -> {card.LocationTo}");
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Unsorted array: ");
            PrintCards(cards);           
            var sorted = Sort.Simple(cards);
            Console.WriteLine("Simple Sorted array: ");
            PrintCards(sorted);            
            Console.ReadLine();
        }
    }
}
