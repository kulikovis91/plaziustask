﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaziusTask
{
    public class Card
    {
        public Card(string from, string to)
        {
            LocationFrom = from;
            LocationTo = to;
        }

        public string LocationFrom { get; set; }
        public string LocationTo { get; set; }
    }
}
