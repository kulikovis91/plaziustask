﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaziusTask
{
    public static class Sort
    {
        /// <summary>
        /// Аналог Selection sort 
        /// без использования дженериков        
        /// сложность O(n^2)
        /// </summary>
        /// <param name="inputCards"></param>
        /// <returns></returns>
        public static Card[] Simple(Card[] inputCards)
        {
            var result = inputCards.ToArray();

            for (var i = 0; i < result.Length - 1; i++)
            {
                for(var j = i + 1; j < result.Length; j++)
                {
                    if (result[i].LocationTo == result[j].LocationFrom)
                    {
                        var tmp = result[i + 1];
                        result[i + 1] = result[j];
                        result[j] = tmp;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Сортировка с использованием дотнетовских
        /// дженериков. После нахождения каждой карточки 
        /// она удаляется из списка
        /// сложность O(n^2)
        /// </summary>
        /// <param name="inputCardsList"></param>
        /// <returns></returns>
        public static List<Card> DotnetGenerics(List<Card> inputCardsList)
        {
            var result = new List<Card>(inputCardsList.Count) {inputCardsList.First()};

            inputCardsList.Remove(inputCardsList.First());

            while (inputCardsList.Any())
                foreach (var c in inputCardsList)
                {
                    if (c.LocationFrom == result.Last().LocationTo)
                    {
                        result.Add(c);
                        inputCardsList.Remove(c);
                        break;
                    }
                }            

            return result;
        }
    }
}
