﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaziusTask;

namespace PlaziusTaskTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Default()
        {
           Card[] cards = {
                new Card("Мельбурн", "Кельн"),
                new Card("Москва", "Париж"),
                new Card("Кельн", "Москва"),
                new Card("Рязань", "Щегры"),
                new Card("Прага", "Рязань"),
                new Card("Щегры", "Одинцово"),
                new Card("Париж", "Прага")
            };

            var result = Sort.Simple(cards);

            Assert.AreSame(result[1], cards[2]);
            Assert.AreSame(result[2], cards[1]);
            Assert.AreSame(result[3], cards[6]);
            Assert.AreSame(result[4], cards[4]);
            Assert.AreSame(result[5], cards[3]);
            Assert.AreSame(result[6], cards[5]);            
        }

        [TestMethod]
        public void CheckContinuity()
        {
            Card[] cards = {
                new Card("Мельбурн", "Кельн"),
                new Card("Москва", "Париж"),
                new Card("Кельн", "Москва"),
                new Card("Рязань", "Щегры"),
                new Card("Прага", "Рязань"),
                new Card("Щегры", "Одинцово"),
                new Card("Париж", "Прага")
            };

            var result = Sort.Simple(cards);

            for (var i = 0; i < result.Count() - 1; i++)
            {
                Assert.AreEqual(result[i].LocationTo, result[i + 1].LocationFrom);
            }
        }
        
        [TestMethod]
        public void CheckLoops()
        {
            Card[] cards = {
                new Card("Мельбурн", "Кельн"),
                new Card("Москва", "Париж"),
                new Card("Кельн", "Москва"),
                new Card("Рязань", "Щегры"),
                new Card("Прага", "Рязань"),
                new Card("Щегры", "Одинцово"),
                new Card("Париж", "Прага")
            };

            var result = Sort.Simple(cards);

            if (result.Select(l => l.LocationFrom).Distinct().Count() < result.Count() 
                || result.Select(l => l.LocationFrom).Distinct().Count() < result.Count())
            {
                Assert.Fail("loop");
            }            
        }        

        [TestMethod]
        public void MissingLink()
        {
            Card[] cards = {
                new Card("Мельбурн", "Кельн"),
                new Card("Москва", "Париж"),
                new Card("Кельн", "Москва"),
                // new Card("Рязань", "Щегры"),
                new Card("Рязань", "Домодедово"), // missing link
                new Card("Прага", "Рязань"),
                new Card("Щегры", "Одинцово"),
                new Card("Париж", "Прага")
            };

            var result = Sort.Simple(cards);

            for (var i = 0; i < result.Count() - 1; i++)
            {
                Assert.AreEqual(result[i].LocationTo, result[i + 1].LocationFrom);
            }
        }
    }
}
